package hxb.cesa_app;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by huxb9_000 on 6/10/2015.
 * This class is reserved for middle layout
 */
public class MiddleFragment extends Fragment {

    private String title;
    private int page;

    public static MiddleFragment newInstance(int page, String title){
        MiddleFragment fragmentMiddle = new MiddleFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentMiddle.setArguments(args);
        return fragmentMiddle;
    }


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        page= getArguments().getInt("somInt", 1);
        title = getArguments().getString("someTitle");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.layout_middle, container, false);
        TextView tvLabel = (TextView) view.findViewById(R.id.tvLabel);
        tvLabel.setText(page + " -- " + title);
        return view;
    }

}
