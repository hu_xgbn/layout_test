package hxb.cesa_app;

import android.content.Context;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;


public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.login_page);
        final Button login_button =(Button) findViewById(R.id.button11);
        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText user = (EditText) findViewById(R.id.usernameinput);
                EditText password = (EditText) findViewById(R.id.passwordtxt);

                if (user.getText().toString().equals("admin") && password.getText().toString().equals("admin")) {
                    set_login_button(v);
                } else {
                    Toast.makeText(getApplicationContext(), "Wrong Credentials", Toast.LENGTH_SHORT).show();
                }

            }});

    }

    public void set_login_button(View v)
    {
        setContentView(R.layout.activity_main);
        /*
        *The logs are for debugging purposes
        */
        Log.e("XXXXXXXXXXXX", "1");
        ViewPager vpPager = (ViewPager) findViewById(R.id.vpPager);
        Log.e("XXXXXXXXXXXX", "2");
        MyPagerAdapter adapterViewPager = new MyPagerAdapter(getSupportFragmentManager());
        Log.e("XXXXXXXXXXXX", "3");
        vpPager.setAdapter(adapterViewPager);

        vpPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {


            }

            @Override
            public void onPageSelected(int i) {
                Toast.makeText(MainActivity.this, "Selected page position: " + i,
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabsStrip.setViewPager(vpPager);
        Log.d("XXXXXXXXXXXX", "OnCreate finished");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class MyPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 2;

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }


        /*
        *This function overridees the original function to instantiate the fragment pages
         */
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return L1Fragment.newInstance(0, "Page # 1");
                case 1:
                    return MiddleFragment.newInstance(1, "Page # 2");
                default:
                    return null;

            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Current Page";

                case 1:
                    return "The Board";

                default:
                    return null;

            }


        }


    }
}


