package com.cesa.cesa_app_beta;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by huxb9_000 on 6/26/2015.
 */
public class Current extends Fragment {

    private int page;
    private String title;
    public static Current newInstance(int page, String title){
         Current currentPage = new Current();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        currentPage.setArguments(args);
        return currentPage;
    }


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        page= getArguments().getInt("somInt", 0);
        title = getArguments().getString("someTitle");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.current_page, container, false);



        return view;
    }
}
