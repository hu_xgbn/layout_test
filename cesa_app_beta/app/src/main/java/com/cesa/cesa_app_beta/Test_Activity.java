package com.cesa.cesa_app_beta;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.widget.ImageView;

/**
 * Created by huxb9_000 on 6/26/2015.
 */
public class Test_Activity extends ActionBarActivity{



    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.current_page);

        Bundle bundle=getIntent().getExtras();
        int dra = bundle.getInt("picture");
        Drawable d = getResources().getDrawable(dra);
        ImageView image = (ImageView)findViewById(R.id.container);
        image.setImageDrawable(d);

    }
}
