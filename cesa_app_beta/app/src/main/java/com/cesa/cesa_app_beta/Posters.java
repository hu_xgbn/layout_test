package com.cesa.cesa_app_beta;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.provider.SyncStateContract;
import android.support.v4.app.Fragment;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

/**
 * Created by huxb9_000 on 6/26/2015.
 */
public class Posters extends Fragment {
    private int page;
    private String title;
    private Integer[] mThumbIds_upper = {
            R.drawable.gumi, R.drawable.gumi2,
            R.drawable.miku, R.drawable.miku2,
            R.drawable.vocaloid1, R.drawable.vocaloid2,
            R.drawable.vocaloid3
    };

    public static Posters newInstance(int page, String title){
        Posters fragmentPosters = new Posters();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentPosters.setArguments(args);
        return fragmentPosters;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        page= getArguments().getInt("somInt", 0);
        title = getArguments().getString("someTitle");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.posters, container, false);



        GridView gridview = (GridView) view.findViewById(R.id.gridview);
        gridview.setAdapter(new ImageAdapter(getActivity()));

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
//                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), mThumbIds_upper[position]);
//                ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
//                byte[] b = baos.toByteArray();
//
                Intent mainIntent = new Intent(getActivity(), Test_Activity.class);
                mainIntent.putExtra("picture", mThumbIds_upper[position]);
                startActivity(mainIntent);

            }
        });

        GridView textgridview = (GridView) view.findViewById(R.id.textgridview);
        textgridview.setAdapter(new TextAdapter(getActivity()));




        return view;
    }

    public class ImageAdapter extends BaseAdapter{
        private Context mContext;
        private long height;


        public ImageAdapter(Context c){
            mContext = c;
            DisplayMetrics metrics = new DisplayMetrics();
            ((Activity) c).getWindowManager().getDefaultDisplay().getMetrics(metrics);

                height = (long)metrics.widthPixels / 4;


        }



        public int getCount(){
            return mThumbIds.length;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            if (convertView == null) {
                // if it's not recycled, initialize some attributes
                imageView = new ImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams((int)height, (int)(height/2*3)));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(8, 8, 8, 8);
            } else {
                imageView = (ImageView) convertView;
            }

            imageView.setImageResource(mThumbIds[position]);
            return imageView;
        }

        private Integer[] mThumbIds = {
                R.drawable.gumi, R.drawable.gumi2,
                R.drawable.miku, R.drawable.miku2,
                R.drawable.vocaloid1, R.drawable.vocaloid2,
                R.drawable.vocaloid3
        };

    }

    public class TextAdapter extends BaseAdapter{
        private Context mContext;
        private long height;


        public TextAdapter(Context c)
        {
            mContext = c;
            DisplayMetrics metrics = new DisplayMetrics();
            ((Activity) c).getWindowManager().getDefaultDisplay().getMetrics(metrics);

            height = (long)metrics.widthPixels / 4;
        }

        @Override
        public int getCount()
        {
            return 4;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            TextView MyView;
            if (convertView == null) {
                // if it's not recycled, initialize some attributes
                MyView = new TextView(mContext);
                MyView.setLayoutParams(new GridView.LayoutParams((int) height, (int) (height / 2 * 3)));
                MyView.setText("I am" + position);
                MyView.setPadding(8, 8, 8, 8);
            } else {
                MyView = (TextView) convertView;
            }


            return MyView;
        }

        @Override
        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }
    }

}

